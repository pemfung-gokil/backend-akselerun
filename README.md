## Setup environment for Hashkell serverless

http://hackage.haskell.org/package/serverless-haskell

## Migrate to Scotty, Haskell web app using Scotty

https://dev.to/parambirs/how-to-write-a-haskell-web-servicefrom-scratch---part-3-5en6

# hello-scotty
A simple haskell web app using scotty

To run:

`$ cabal run`

To build docker image

`% docker build -t pwu .`

To run the application using docker:

`% docker run -i -t -p 3000:3000 pwu`

To view the application in the browser:

`% open http://localhost:3000`


