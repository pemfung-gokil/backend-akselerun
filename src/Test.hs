import Test.HUnit

import System.Exit (exitWith, exitFailure, exitSuccess)

import Matrix

transpose_test_1 = TestCase (assertBool "transpose 1 x 3" (transpose([[1, 2, 3]]) == [[1], [2], [3]]))
transpose_test_2 = TestCase (assertBool "transpose 3 x 1" (transpose([[1], [2], [3]]) == [[1, 2, 3]]))

multiply_test_1 = TestCase (assertBool "multiply 3 x 1" 
    (multiply [[1, 2, 3]] [[1], [2], [3]] == [[14]]))

remove_row_test_1 = TestCase (assertBool "remove row 1"
    (remove_row [[1, 2], [3, 4], [5, 6]] 1 == [[1, 2], [5, 6]]))

remove_col_test_1 = TestCase (assertBool "remove col 1"
    (remove_col [[1, 2, 3], [4, 5, 6]] 1 == [[1, 3], [4, 6]]))

minor_test_1 = TestCase (assertBool "minor 1 0"
    (minor [[1, 2, 3], [4, 5, 6], [7, 8, 9]] 1 0 == [[2, 3], [8, 9]]))

mul_const_test_1 = TestCase (assertBool "multiply with const"
    (mul_const (-1) [[1, 2], [-3, 4]] == [[-1, -2], [3, -4]]))

determinant_test_1 = TestCase (assertBool "determinant 3 x 3"
    (determinant [[1, 2, 3], [2, 1, 4], [3, 1, 2]] == 11))

tests = TestList [transpose_test_1, 
                  transpose_test_2, 
                  multiply_test_1, 
                  remove_row_test_1,
                  remove_col_test_1,
                  minor_test_1,
                  mul_const_test_1,
                  determinant_test_1]

main :: IO ()
main = do
    results <- runTestTT $ tests
    if (errors results + failures results == 0)
      then
        exitSuccess
      else
        exitFailure