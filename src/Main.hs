{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import GHC.Generics
import Web.Scotty
import Network.HTTP.Types
import Data.Aeson (FromJSON, ToJSON)
import Data.Text
import Network.Wai.Middleware.Cors



data Calculator = Calculator { initialDeposit :: Float, monthlyDeposit :: Float, monthDuration :: Int, annualInterestRate :: Float  } deriving (Show, Generic)
instance ToJSON Calculator
instance FromJSON Calculator

data CalculateResult = CalculateResult { result :: Float} deriving (Show, Generic)
instance ToJSON CalculateResult
instance FromJSON CalculateResult


data TestJson = TestJson { val :: Float } deriving (Show, Generic)
instance ToJSON TestJson
instance FromJSON TestJson


f a  = a * 2

main = scotty 3002 $ do
  middleware simpleCors


  post "/tests" $ do
    rawJson <- jsonData :: ActionM Calculator
    let initDepo = initialDeposit rawJson
    let monthDepo = monthlyDeposit rawJson
    let montDur = monthDuration rawJson
    let annRate = annualInterestRate rawJson

    -- let test = [[row !! 1] | row <- initDepo]
    -- let result = JsonFormat {result = (f test)}
    let test = initDepo * monthDepo / 2
    let result = TestJson {val = test}
    json result

  